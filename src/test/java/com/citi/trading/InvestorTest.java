package com.citi.trading;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;

// cash,
public class InvestorTest {

	private static OrderPlacer Market_Mock;
	private static Investor investor;
	
	
	public class marketMock implements OrderPlacer{
//		public marketMock() {
//		//pass	
//		}
		
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			callback.accept(order);

		}
		
	}
	
	@Before
	public void setup() {
		Market_Mock = new marketMock();
		investor = new Investor(10000);
		investor.setMarket(Market_Mock);

	}
	
	@Test
	public void testInvestorBuy() {
		// stock size prize
		investor.buy("MRK", 100, 60);
		assertThat(investor.getCash(), closeTo(4000, 0.0001));	
		assertThat(investor.getPortfolio(), hasEntry("MRK",100));
	}
	
	// sell a stock which you have bought earlier
	@Test
	public void testInvestorSell() {
//		investor.buy("MRK", 100, 60);
		Map<String,Integer> portfolio = new HashMap<>();
		portfolio.put("MRK", 100);
		investor = new Investor(portfolio,10000);
		investor.setMarket(Market_Mock);
		investor.sell("MRK", 100,100 );
		assertThat(investor.getPortfolio(), not(hasEntry("MRK",100)));
		assertThat(investor.getCash(), closeTo(20000, 0.0001));	// check our money increased
		
	}
	
	//selling a stock which you don't have [ NEED TO FIX THE CODE ] 
//	@Test
//	public void testInvestorSellWithNoStock() {
//		investor.sell("MRK", 100, 60);
//		assertThat(investor.getCash(), closeTo(10000, 0.000001));	
//	}
	
	@Test
	public void testGetCash() {
		// the investory was intialized above
		assertThat(investor.getCash(), closeTo(10000,0.0001));
	}
	
	// check the getPortfolio method works
	@Test 
	public void testGetPortfolio() {
		assertThat(investor.getPortfolio(), equalTo(new HashMap<>()));
	}
	
	@Test
	public void testPortfolioHasEntry() {
		investor.buy("MRK", 100, 60);
		assertThat(investor.getPortfolio(), hasEntry("MRK",100));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeSizeorPrize() {
		investor.buy("MRK", -100, 60);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNotEnoughMoney() {
		investor.buy("MRK", 100000, 60);
	}
	
	
//	haskey and hasvalue for portfolio

}
